#!/usr/bin/env python3
"""A module that implements a test harness for the Collatz module."""

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C)
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase, expectedFailure

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz(TestCase):
    """A set of unit tests to run on the Collatz module."""
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 1)
        self.assertEqual(j, 10)

    def test_read_2(self):
        s = "100                          200\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 100)
        self.assertEqual(j, 200)

    def test_read_3(self):
        s = "201 \n 210\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 201)
        self.assertEqual(j, 210)

    def test_read_4(self):
        s = "900 1000\n\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 900)
        self.assertEqual(j, 1000)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    def test_eval_5(self):
        v = collatz_eval(10, 1)
        self.assertEqual(v, 20)

    @expectedFailure
    def test_eval_6(self):
        collatz_eval(0, 0)

    def test_eval_7(self):
        v = collatz_eval(1, 1)
        self.assertEqual(v, 1)

    def test_eval_8(self):
        v = collatz_eval(2000, 5002)
        self.assertEqual(v, 238)

    def test_eval_9(self):
        v = collatz_eval(3002, 6002)
        self.assertEqual(v, 238)

    def test_eval_10(self):
        v = collatz_eval(28001, 31500)
        self.assertEqual(v, 285)

    def test_eval_11(self):
        v = collatz_eval(27500, 31000)
        self.assertEqual(v, 272)

    @expectedFailure
    def test_eval_12(self):
        collatz_eval(1.5, 10.5)

    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    def test_print_2(self):
        w = StringIO()
        collatz_print(w, 9238749837234, 20394820, 92379857984375987349594)
        self.assertEqual(
            w.getvalue(), "9238749837234 20394820 92379857984375987349594\n")

    def test_print_3(self):
        w = StringIO()
        collatz_print(w, -1, -34510, -983749587439857920)
        self.assertEqual(w.getvalue(), "-1 -34510 -983749587439857920\n")

    @expectedFailure
    def test_print_4(self):
        collatz_print(5, 1, 10, 20)

    @expectedFailure
    def test_print_5(self):
        w = StringIO()
        collatz_print(w, "one", "two", "one billion")

    @expectedFailure
    def test_print_6(self):
        w = StringIO()
        collatz_print(w, 1.0, 5.55555, 92834234.2394e3)

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

    def test_solve_2(self):
        r = StringIO(
            "1         10\n100          200\n201        210\n900          1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

    def test_solve_3(self):
        r = StringIO(
            "1 10\n\n\n\n100 200\n\n\n\n201 210\n\n\n\n900 1000\n\n\n\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

    def test_solve_4(self):
        r = StringIO("1 10\t\n100 200\t\n201 210\t\n900 1000\t\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

    def test_solve_5(self):
        r = StringIO("1 10\r\n100 200\r\n201 210\r\n900 1000\r\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

    def test_solve_6(self):
        r = StringIO("1 10\r\n100 200\r\n201 210\r\n900 1000\r\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

    # @expectedFailure
    def test_solve_7(self):
        r = StringIO("1 10 523\r\n100 200\r\n201 210\r\n900 1000\r\n")
        w = StringIO()
        collatz_solve(r, w)

# ----
# main
# ----


if __name__ == "__main__":  #pragma: no cover
    main()
